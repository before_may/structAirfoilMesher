!  This file is part of structAirfoilMesher.

!  structAirfoilMesher is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.

!  structAirfoilMesher is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.

!  You should have received a copy of the GNU General Public License
!  along with structAirfoilMesher.  If not, see <http://www.gnu.org/licenses/>.

!  Parts of this file were originally part of Construct2D
!  https://sourceforge.net/projects/construct2d
!  author: Daniel Prosser
!  Copyright (C) 2013 -- 2018

!  File creation and modifications for structAirfoilMesher
!  author: Konstantinos Diakakis
module plot3dOutput

    implicit none

contains

    subroutine writeGridPlot3d(grid, opt)

        Use vardef, only: srf_grid_type, options_type

        type(srf_grid_type), intent(in) :: grid
        type(options_type), intent(in) :: opt

        integer i, j, k
        integer imaxTotal, jmaxTotal, kmaxTotal
        integer imax, jmax, kmax
        integer lvl, step
        logical threed

        double precision :: angle, deltaX
        double precision :: term1, term2, term, zCurrent

        character filename*64
        character levelstr*6

        threed = .false.
        if (opt%griddim==3 .and. opt%nplanes>1) threed = .true.

        imaxTotal = grid%imax
        jmaxTotal = grid%jmax
        if (threed) then
            kmaxTotal = opt%nplanes
        else
            kmaxTotal = 1
        end if

        if (opt%multiGridLevels.gt.0) then
            if (mod(imaxTotal - 1, 2**opt%multiGridLevels).ne.0) then
                write (*, *) 'Grid (imax-1) cannot be divided by multigrid levels.'
                stop
            elseif (mod(jmaxTotal - 1, 2**opt%multiGridLevels).ne.0) then
                write (*, *) 'Grid (jmax-1) cannot be divided by multigrid levels.'
                stop
            elseif ((mod(opt%nplanes - 1, 2**opt%multiGridLevels).ne.0) .and. (threed)) then
                write (*, *) 'Grid (npln-1) cannot be divided by multigrid levels.'
                stop
            end if
        end if

        if (opt%spanwiseMethod.eq.'CLRD') then
            if (mod(opt%nplanes - 1, 2).ne.0) then
                write (*, *) 'Grid (npln-1) cannot be divided by 2. Clustered meshing will not work correctly.'
                stop
            end if
        end if

        do lvl = 0, opt%multiGridLevels

            if (opt%multiGridLevels.gt.0) then
                write (*, *) 'Multigrid Level: ', lvl

                write (levelstr, '(a5,i1)') 'Level', lvl
                filename = trim(opt%projectName)//trim(levelstr)//'.p3d'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            else
                filename = trim(opt%projectName)//'.p3d'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            end if

            imax = (imaxTotal - 1)/2**lvl + 1
            jmax = (jmaxTotal - 1)/2**lvl + 1
            if (threed) then
                kmax = (opt%nplanes - 1)/2**lvl + 1
            else
                kmax = 1
            end if

            open (1, file=trim(filename))
            step = 2**(lvl)

            ! Write header to output file

            if (threed) then
                write (1, *) 1
                write (1, *) imax, jmax, kmax
            else
                write (1, *) imax, jmax
            end if

            ! Write out grid - in reverse i direction to preserve positive volumes

            if (threed) then

                do k = 0, kmax-1 ! swtiching indexing from 1 to kmax to 0 to kmax-1 in order to have z=0 as the start

                    if (opt%spanwiseMethod.eq.'UNFM') then
                        zCurrent = dble(k*step)*opt%spanwiseSpacing
                    else
                        if (k.eq.0) then
                            zCurrent = 0.d0
                        else
                            term1 = tanh(opt%zeta*(2.d0*k/(kmax - 1) - 1.d0))
                            term2 = tanh(opt%zeta)
                            term = (1.d0 + term1/term2)
                            zCurrent = (opt%span/2.d0)*term
                        end if
                    end if

                    if (opt%alphaSweep.gt.0.d0) then
                        angle = opt%alphaSweep*4.d0*atan(1.d0)/180.d0
                        deltaX = tan(angle)*zCurrent
                    else
                        deltaX = 0.d0
                    end if

                    do j = 1, jmax
                        do i = imax, 1, -1
                            write (1, '(es17.8)') grid%x((i - 1)*step + 1, (j - 1)*step + 1) + deltaX
                        end do
                    end do

                end do

                do k = 1, kmax ! swtiching indexing does not matter here, y is not affected by spanwise index
                    do j = 1, jmax
                        do i = imax, 1, -1
                            write (1, '(es17.8)') grid%y((i - 1)*step + 1, (j - 1)*step + 1)
                        end do
                    end do
                end do

                do k = 0, kmax-1 ! again swtiching indexing from 1 to kmax to 0 to kmax-1 in order to have z=0 as the start

                    if (opt%spanwiseMethod.eq.'UNFM') then
                        zCurrent = dble((k - 1)*step)*opt%spanwiseSpacing
                    else
                        if (k.eq.0) then
                            zCurrent = 0.d0
                        else
                            term1 = tanh(opt%zeta*(2.d0*k/(kmax - 1) - 1.d0))
                            term2 = tanh(opt%zeta)
                            term = (1.d0 + term1/term2)
                            zCurrent = (opt%span/2.d0)*term
                        end if
                    end if

                    do j = 1, jmax
                        do i = imax, 1, -1
                            write (1, '(es17.8)') dble((k - 1)*step)*opt%spanwiseSpacing
                        end do
                    end do

                end do

            else

                do j = 1, jmax
                    do i = imax, 1, -1
                        write (1, '(es17.8)') grid%x((i - 1)*step + 1, (j - 1)*step + 1)
                    end do
                end do

                do j = 1, jmax
                    do i = imax, 1, -1
                        write (1, '(es17.8)') grid%y((i - 1)*step + 1, (j - 1)*step + 1)
                    end do
                end do

            end if

            close (1)

        end do

    end subroutine writeGridPlot3d

!=============================================================================80
!
! Subroutine to write out grid quality stats in plot3d format
!
!=============================================================================80
    subroutine writeQualityStatsPlot3d(iunit, qstats, griddim, nplanes)

        Use vardef, only: grid_stats_type

        type(grid_stats_type), intent(in) :: qstats
        integer, intent(in) :: iunit, griddim, nplanes

        integer imax, jmax, kmax, i, j, k
        logical threed

        imax = size(qstats%ang1, 1)
        jmax = size(qstats%ang1, 2)
        kmax = nplanes

        threed = .false.
        if (griddim==3 .and. nplanes>1) threed = .true.

! Write comments giving title and variables

        write (iunit, '(A)') '#Grid quality information'
        write (iunit, '(A)') '#skew angle, xi-growth, eta-growth'

! Write Plot3D header

        if (threed) then
            write (iunit, *) imax, kmax, jmax, 3
        else
            write (iunit, *) imax, jmax, 1, 3
        end if

! Write out grid stats - in reverse order to preserve positive volumes

        if (threed) then

            do j = 1, jmax
            do k = 1, kmax
            do i = imax, 1, -1
                write (iunit, '(es17.8)') qstats%skewang(i, j)
            end do
            end do
            end do

            do j = 1, jmax
            do k = 1, kmax
            do i = imax, 1, -1
                write (iunit, '(es17.8)') qstats%growthz(i, j)
            end do
            end do
            end do

            do j = 1, jmax
            do k = 1, kmax
            do i = imax, 1, -1
                write (iunit, '(es17.8)') qstats%growthn(i, j)
            end do
            end do
            end do

        else

            write (iunit, '(es17.8)') &
                ((qstats%skewang(i, j), i=imax, 1, -1), j=1, jmax), &
                ((qstats%growthz(i, j), i=imax, 1, -1), j=1, jmax), &
                ((qstats%growthn(i, j), i=imax, 1, -1), j=1, jmax)

        end if

    end subroutine writeQualityStatsPlot3d

!=============================================================================80
!
! Subroutine to write boundary conditions file (.nmf format)
!
!=============================================================================80
    subroutine writeBCFileNmf(iunit, options)

        Use vardef, only: options_type

        integer, intent(in) :: iunit
        type(options_type), intent(in) :: options

        integer :: imax, jmax, kmax, i

        character(90) :: text1
        logical :: threed

        threed = .false.
        if (options%griddim==3 .and. options%nplanes>1) threed = .true.

! Determine grid dimensions

        if (threed) then
            imax = options%imax
            jmax = options%nplanes
            kmax = options%jmax
        else
            imax = options%imax
            jmax = options%jmax
            kmax = 1
        end if

! Write main header

        write (iunit, '(A)'), '# ==================== '// &
            'Neutral Map File generated by structAirfoilMesher '// &
            '===================='
        write (iunit, '(A)'), '# ==================== '// &
            '========================================= '// &
            '===================='
        write (iunit, '(A)'), '# Block#   IDIM   JDIM   KDIM'
        write (iunit, '(A)'), '# ---------------------'// &
            '------------------------------------------'// &
            '--------------------'

! Write grid dimensions

        do i = 1, 90
            text1(i:i) = ' '
        end do
        call placeIntegerInString(1, text1, 8)
        write (iunit, '(A)'), trim(text1)
        write (iunit, '(A)')
        do i = 1, 90
            text1(i:i) = ' '
        end do
        call placeIntegerInString(1, text1, 8)
        call placeIntegerInString(imax, text1, 15)
        call placeIntegerInString(jmax, text1, 22)
        call placeIntegerInString(kmax, text1, 29)
        write (iunit, '(A)'), trim(text1)
        write (iunit, '(A)')

! Write second header
        write (iunit, '(A)'), '# ====================='// &
            '=========================================='// &
            '===================='
        do i = 3, 90
            text1(i:i) = ' '
        end do
        text1(1:2) = '# '
        call placeSubstringInString('Type', text1, 6)
        call placeSubstringInString('B1', text1, 17)
        call placeSubstringInString('F1', text1, 21)
        call placeSubstringInString('S1', text1, 28)
        call placeSubstringInString('E1', text1, 33)
        call placeSubstringInString('S2', text1, 40)
        call placeSubstringInString('E2', text1, 45)
        call placeSubstringInString('B2', text1, 51)
        call placeSubstringInString('F2', text1, 55)
        call placeSubstringInString('S1', text1, 62)
        call placeSubstringInString('E1', text1, 67)
        call placeSubstringInString('S2', text1, 74)
        call placeSubstringInString('E2', text1, 79)
        call placeSubstringInString('Swap', text1, 85)
        write (iunit, '(A)'), trim(text1)
        do i = 3, 90
            text1(i:i) = ' '
        end do
        if (.not. options%fun3DCompatibility) then
            text1(1:2) = '# '
            call placeSubstringInString('Compute forces (walls)', text1, 85)
            write (iunit, '(A)'), trim(text1)
        end if
        write (iunit, '(A)'), '# ---------------------'// &
            '------------------------------------------'// &
            '--------------------'

! Write boundary conditions

        do i = 1, 90
            text1(i:i) = ' '
        end do

        dimensions: if (threed) then

            topology_3d: if (options%topology=='OGRD') then

!     Face number 1: k = kmin

                if (.not. options%fun3DCompatibility) then
                    text1(1:7) = 'VISCOUS'
                else
                    text1(1:13) = 'viscous_solid'
                end if
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(1, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(imax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(jmax, text1, 45)
                if (.not. options%fun3DCompatibility) then
                    call placeSubstringInString('TRUE', text1, 85)
                end if
                write (iunit, '(A)'), trim(text1)

!     Face number 2: k = kmax

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                if (.not. options%fun3DCompatibility) then
                    text1(1:8) = 'FARFIELD'
                else
                    text1(1:13) = 'farfield_riem'
                end if
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(2, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(imax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(jmax, text1, 45)
                write (iunit, '(A)'), trim(text1)

!     Face number 3: i = imin (one_to_one with face 4)

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                text1(1:10) = 'ONE_TO_ONE'
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(3, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(jmax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                call placeIntegerInString(1, text1, 51)
                call placeIntegerInString(4, text1, 55)
                call placeIntegerInString(1, text1, 62)
                call placeIntegerInString(jmax, text1, 67)
                call placeIntegerInString(1, text1, 74)
                call placeIntegerInString(kmax, text1, 79)
                call placeSubstringInString('FALSE', text1, 85)
                write (iunit, '(A)'), trim(text1)

!     Face number 5: j = jmin

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                if (.not. options%fun3DCompatibility) then
                    text1(1:10) = 'SYMMETRY-Y'
                else
                    text1(1:10) = 'symmetry_y'
                end if
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(5, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(kmax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(imax, text1, 45)
                write (iunit, '(A)'), trim(text1)

!     Face number 6: j = jmax

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                if (.not. options%fun3DCompatibility) then
                    text1(1:10) = 'SYMMETRY-Y'
                else
                    text1(1:10) = 'symmetry_y'
                end if
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(6, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(kmax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(imax, text1, 45)
                write (iunit, '(A)'), trim(text1)

                else topology_3d

!     Face number 1: k = kmin, (one_to_one mapping at cut)

                text1(1:10) = 'ONE_TO_ONE'
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(1, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(options%nwake + 1, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(jmax, text1, 45)
                call placeIntegerInString(1, text1, 51)
                call placeIntegerInString(1, text1, 55)
                call placeIntegerInString(imax, text1, 62)
                call placeIntegerInString(imax - options%nwake, text1, 67)
                call placeIntegerInString(1, text1, 74)
                call placeIntegerInString(jmax, text1, 79)
                call placeSubstringInString('FALSE', text1, 85)
                write (iunit, '(A)'), trim(text1)

!     Face number 1: k = kmin, airfoil surface

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                if (.not. options%fun3DCompatibility) then
                    text1(1:7) = 'VISCOUS'
                else
                    text1(1:13) = 'viscous_solid'
                end if
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(1, text1, 21)
                call placeIntegerInString(options%nwake + 1, text1, 28)
                call placeIntegerInString(imax - options%nwake, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(jmax, text1, 45)
                if (.not. options%fun3DCompatibility) then
                    call placeSubstringInString('TRUE', text1, 85)
                end if
                write (iunit, '(A)'), trim(text1)

!     Face number 2: k = kmax

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                if (.not. options%fun3DCompatibility) then
                    text1(1:8) = 'FARFIELD'
                else
                    text1(1:13) = 'farfield_riem'
                end if
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(2, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(imax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(jmax, text1, 45)
                write (iunit, '(A)'), trim(text1)

!     Face number 3: i = imin (outlet)

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                if (.not. options%fun3DCompatibility) then
                    text1(1:8) = 'FARFIELD'
                else
                    text1(1:13) = 'farfield_riem'
                end if
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(3, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(jmax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                write (iunit, '(A)'), trim(text1)

!     Face number 4: i = imax (outlet)

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                if (.not. options%fun3DCompatibility) then
                    text1(1:8) = 'FARFIELD'
                else
                    text1(1:13) = 'farfield_riem'
                end if
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(4, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(jmax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                write (iunit, '(A)'), trim(text1)

!     Face number 5: j = jmin

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                if (.not. options%fun3DCompatibility) then
                    text1(1:10) = 'SYMMETRY-Y'
                else
                    text1(1:10) = 'symmetry_y'
                end if
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(5, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(kmax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(imax, text1, 45)
                write (iunit, '(A)'), trim(text1)

!     Face number 6: j = jmax

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                if (.not. options%fun3DCompatibility) then
                    text1(1:10) = 'SYMMETRY-Y'
                else
                    text1(1:10) = 'symmetry_y'
                end if
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(6, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(kmax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(imax, text1, 45)
                write (iunit, '(A)'), trim(text1)

            end if topology_3d

            else dimensions

            topology_2d: if (options%topology=='OGRD') then

!     Face number 1: i = imin (one_to_one with face 2)

                text1(1:10) = 'ONE_TO_ONE'
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(1, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(jmax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                call placeIntegerInString(1, text1, 51)
                call placeIntegerInString(2, text1, 55)
                call placeIntegerInString(1, text1, 62)
                call placeIntegerInString(jmax, text1, 67)
                call placeIntegerInString(1, text1, 74)
                call placeIntegerInString(kmax, text1, 79)
                call placeSubstringInString('FALSE', text1, 85)
                write (iunit, '(A)'), trim(text1)

!     Face number 3: j = jmin

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                text1(1:7) = 'VISCOUS'
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(3, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(imax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                call placeSubstringInString('TRUE', text1, 85)
                write (iunit, '(A)'), trim(text1)

!     Face number 4: j = jmax

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                text1(1:8) = 'FARFIELD'
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(4, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(imax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                write (iunit, '(A)'), trim(text1)

                else topology_2d

!     Face number 1: i = imin (outlet)

                text1(1:8) = 'FARFIELD'
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(1, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(jmax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                write (iunit, '(A)'), trim(text1)

!     Face number 2: i = imax (outlet)

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                text1(1:8) = 'FARFIELD'
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(2, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(jmax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                write (iunit, '(A)'), trim(text1)

!     Face number 3: j = jmin (one_to_one mapping at cut)

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                text1(1:10) = 'ONE_TO_ONE'
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(3, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(options%nwake + 1, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                call placeIntegerInString(1, text1, 51)
                call placeIntegerInString(3, text1, 55)
                call placeIntegerInString(imax, text1, 62)
                call placeIntegerInString(imax - options%nwake, text1, 67)
                call placeIntegerInString(1, text1, 74)
                call placeIntegerInString(kmax, text1, 79)
                call placeSubstringInString('FALSE', text1, 85)
                write (iunit, '(A)'), trim(text1)

!     Face number 3: j = jmin (airfoil surface)

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                text1(1:7) = 'VISCOUS'
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(3, text1, 21)
                call placeIntegerInString(options%nwake + 1, text1, 28)
                call placeIntegerInString(imax - options%nwake, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                call placeSubstringInString('TRUE', text1, 85)
                write (iunit, '(A)'), trim(text1)

!     Face number 4: j = jmax (farfield)

                do i = 1, 90
                    text1(i:i) = ' '
                end do

                text1(1:8) = 'FARFIELD'
                call placeIntegerInString(1, text1, 17)
                call placeIntegerInString(4, text1, 21)
                call placeIntegerInString(1, text1, 28)
                call placeIntegerInString(imax, text1, 33)
                call placeIntegerInString(1, text1, 40)
                call placeIntegerInString(kmax, text1, 45)
                write (iunit, '(A)'), trim(text1)

            end if topology_2d

        end if dimensions

    end subroutine writeBCFileNmf

!=============================================================================80
!
! Replaces text in a string with a (positive) integer
! Placement is determined by index of last character to be replaced
!
!=============================================================================80
    subroutine placeIntegerInString(val, string, rindex)

        integer, intent(in) :: val, rindex
        character(*), intent(inout) :: string

        integer :: intlen, lindex
        character(20) :: text

! Determine length of integer

        intlen = ceiling(log10(dble(val) + 0.1))

! Determine first index to replace

        lindex = rindex - intlen + 1

! Write integer as a string

        write (text, *) val
        text = adjustl(text)

! Place in string

        string(lindex:rindex) = trim(text)

    end subroutine placeIntegerInString

!=============================================================================80
!
! Replaces text in a string with a substring
! Placement is determined by index of last character to be replaced
!
!=============================================================================80
    subroutine placeSubstringInString(substring, string, rindex)

        integer, intent(in) :: rindex
        character(*), intent(in) :: substring
        character(*), intent(inout) :: string

        integer :: sublen, lindex

! Determine length of substring

        sublen = len(substring)

! Determine first index to replace

        lindex = rindex - sublen + 1

! Place in string

        string(lindex:rindex) = trim(substring)

    end subroutine placeSubstringInString

end module plot3dOutput
