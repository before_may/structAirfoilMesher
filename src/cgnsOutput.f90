!  This file is part of structAirfoilMesher.

!  structAirfoilMesher is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.

!  structAirfoilMesher is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.

!  You should have received a copy of the GNU General Public License
!  along with structAirfoilMesher.  If not, see <http://www.gnu.org/licenses/>.

!  author: Konstantinos Diakakis
module cgnsOutput

    implicit none

contains

    subroutine writeGridCgnsUnstructured(grid, opt)

        use cgns
        Use vardef, only: srf_grid_type, options_type

        type(srf_grid_type), intent(in) :: grid
        type(options_type), intent(in) :: opt

        integer ier

        integer imax, jmax, kmax
        integer imaxTotal, jmaxTotal, kmaxTotal
        integer i, j, k, iReverse
        integer iLowAirfoil, iHighAirfoil
        integer step
        integer icelldim, iphysdim
        integer lvl
        integer nodeIndex
        logical threed

        integer(cgsize_t) isize(1, 3)

        !----
        ! CGNS indices
        !----
        integer index_file
        integer index_base, index_zone, index_coord, index_section, index_bc
        integer index_family, index_familybc

        !----
        ! nodes
        !----
        double precision, allocatable :: x_out(:), y_out(:), z_out(:)
        double precision :: angle, deltaX

        !----
        ! elements
        !----
        integer(CGSIZE_T), allocatable :: hexaElem(:, :)
        integer(CGSIZE_T), allocatable :: quadElem(:, :)
        integer(CGSIZE_T), allocatable :: barElem(:, :)
        integer :: elemIndex
        integer :: firstNodeIndex
        integer(CGSIZE_T) :: nElemStart, nElemEnd
        integer :: nBdyElem
        integer :: nQuadsLoc
        integer :: nBarsLoc

        !----
        ! boundary conditions
        !----
        integer(CGSIZE_T), allocatable :: bcPnts(:)
        integer(CGSIZE_T) :: bcCount

        character basename*32, zonename*32
        character bndName*32
        character filename*64
        character levelstr*6

        threed = .false.
        if (opt%griddim==3 .and. opt%nplanes>1) threed = .true.

        imaxTotal = grid%imax
        jmaxTotal = grid%jmax
        if (threed) then
            kmaxTotal = opt%nplanes
        else
            kmaxTotal = 1
        end if

        if (opt%multiGridLevels.gt.0) then
            if (mod(imaxTotal - 1, 2**opt%multiGridLevels).ne.0) then
                write (*, *) 'Grid (imax-1) cannot be divided by multigrid levels.'
                stop
            elseif (mod(jmaxTotal - 1, 2**opt%multiGridLevels).ne.0) then
                write (*, *) 'Grid (jmax-1) cannot be divided by multigrid levels.'
                stop
            elseif ((mod(opt%nplanes - 1, 2**opt%multiGridLevels).ne.0) .and. (threed)) then
                write (*, *) 'Grid (npln-1) cannot be divided by multigrid levels.'
                stop
            end if
        end if

        if (opt%spanwiseMethod.eq.'CLRD') then
            if (mod(opt%nplanes - 1, 2).ne.0) then
                write (*, *) 'Grid (npln-1) cannot be divided by 2. Clustered meshing will not work correctly.'
                stop
            end if
        end if

        do lvl = 0, opt%multiGridLevels

            !----
            ! set output filename
            !----
            if (opt%multiGridLevels.gt.0) then
                write (*, *) 'Multigrid Level: ', lvl
                write (levelstr, '(a5,i1)') 'Level', lvl
                filename = trim(opt%projectName)//trim(levelstr)//'.cgns'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            else
                filename = trim(opt%projectName)//'.cgns'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            end if
            !----
            ! define i-j-k bounds
            !----
            imax = (imaxTotal - 1)/2**lvl + 1
            jmax = (jmaxTotal - 1)/2**lvl + 1
            if (threed) then
                kmax = (opt%nplanes - 1)/2**lvl + 1
            else
                kmax = 1
            end if
            !----
            ! open CGNS file
            !----
            call cg_open_f(trim(filename), CG_MODE_WRITE, index_file, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f
            !----
            ! write CGNS Base node
            !----
            call writeCgnsBase
            !----
            ! write CGNS Zone node
            !----
            call writeCgnsZone
            !----
            ! write node coordinates
            !----
            call writeCgnsNodeCoor
            !----
            ! write volume elements
            !----
            call writeCgnsVolumeElements
            !----
            ! write boundary element sections and boundary conditions
            ! -- based on O-type and C-type, the mesh boundaries have different physical meaning
            !----
            call writeCgnsBoundaryLowI
            call writeCgnsBoundaryHighI
            if (opt%topology.eq.'OGRD') call writeCgnsBoundaryLowJOType
            if (opt%topology.eq.'CGRD') call writeCgnsBoundaryLowJCType
            call writeCgnsBoundaryHighJ
            if (threed) then
                call writeCgnsBoundaryLowK
                call writeCgnsBoundaryHighK
            end if
            !----
            ! close CGNS file
            !----
            call cg_close_f(index_file, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end do

    contains

        Subroutine writeCgnsBase
            !----
            ! base
            !----
            basename = 'Base'
            if (threed) then
                icelldim = 3
                iphysdim = 3
            else
                icelldim = 2
                iphysdim = 2
            end if
            call cg_base_write_f(index_file, basename, icelldim, iphysdim, index_base, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end subroutine writeCgnsBase

        subroutine writeCgnsZone

            zonename = 'Zone'

            ! nodes
            isize(1, 1) = imax*jmax*kmax
            ! cells
            if (threed) isize(1, 2) = (imax - 1)*(jmax - 1)*(kmax - 1)
            if (.not. threed) isize(1, 2) = (imax - 1)*(jmax - 1)
            ! boundary
            isize(1, 3) = 0

            call cg_zone_write_f(index_file, index_base, zonename, isize, Unstructured, index_zone, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end subroutine writeCgnsZone

        subroutine writeCgnsNodeCoor

            double precision :: term1, term2, term, zCurrent

            allocate (x_out(imax*jmax*kmax), y_out(imax*jmax*kmax), z_out(imax*jmax*kmax))

            step = 2**(lvl)

            nodeIndex = 0
            do k = 1, kmax

                if (opt%spanwiseMethod.eq.'UNFM') then
                    zCurrent = dble((k - 1)*step)*opt%spanwiseSpacing
                else
                    if (k.eq.0) then
                        zCurrent = 0.d0
                    else
                        term1 = tanh(opt%zeta*(2.d0*k/(kmax - 1) - 1.d0))
                        term2 = tanh(opt%zeta)
                        term = (1.d0 + term1/term2)
                        zCurrent = (opt%span/2.d0)*term
                    end if
                end if

                if (opt%alphaSweep.gt.0.d0) then
                    angle = opt%alphaSweep*4.d0*atan(1.d0)/180.d0
                    deltaX = tan(angle)*zCurrent
                else
                    deltaX = 0.d0
                end if

                do j = 1, jmax
                    do i = 1, imax
                        iReverse = imax + 1 - i ! x and y are in reverse i order to preserve positive volumes
                        nodeIndex = nodeIndex + 1
                        if (threed) then
                            x_out(nodeIndex) = grid%x((iReverse - 1)*step + 1, (j - 1)*step + 1) + deltaX
                            y_out(nodeIndex) = grid%y((iReverse - 1)*step + 1, (j - 1)*step + 1)
                            z_out(nodeIndex) = zCurrent
                        else
                            x_out(nodeIndex) = grid%x((iReverse - 1)*step + 1, (j - 1)*step + 1)
                            y_out(nodeIndex) = grid%y((iReverse - 1)*step + 1, (j - 1)*step + 1)
                            z_out(nodeIndex) = 0.d0
                        end if
                    end do
                end do

            end do

            call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateX', x_out, index_coord, ier)
            call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateY', y_out, index_coord, ier)
            if (threed) call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateZ', z_out, index_coord, ier)

            deallocate (x_out, y_out, z_out)

        end subroutine writeCgnsNodeCoor

        subroutine writeCgnsVolumeElements

            if (threed) then

                !----
                ! hexas
                !----
                allocate (hexaElem(8, (imax - 1)*(jmax - 1)*(kmax - 1)))

                nElemStart = 1
                elemIndex = 0
                do k = 1, kmax - 1
                    do j = 1, jmax - 1
                        do i = 1, imax - 1

                            elemIndex = elemIndex + 1

                            firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                            hexaElem(1, elemIndex) = firstNodeIndex
                            hexaElem(2, elemIndex) = firstNodeIndex + 1
                            hexaElem(3, elemIndex) = firstNodeIndex + 1 + imax
                            hexaElem(4, elemIndex) = firstNodeIndex + imax
                            hexaElem(5, elemIndex) = firstNodeIndex + imax*jmax
                            hexaElem(6, elemIndex) = firstNodeIndex + imax*jmax + 1
                            hexaElem(7, elemIndex) = firstNodeIndex + imax*jmax + 1 + imax
                            hexaElem(8, elemIndex) = firstNodeIndex + imax*jmax + imax

                        end do
                    end do
                end do
                nElemEnd = elemIndex
                nBdyElem = 0
                call cg_section_write_f(index_file, index_base, index_zone, 'HexaElements', HEXA_8, nElemStart, nElemEnd, nBdyElem, hexaElem, index_section, ier)

                deallocate (hexaElem)

            else

                !----
                ! hexas
                !----
                allocate (quadElem(4, (imax - 1)*(jmax - 1)))

                nElemStart = 1
                elemIndex = 0
                do j = 1, jmax - 1
                    do i = 1, imax - 1

                        elemIndex = elemIndex + 1

                        firstNodeIndex = i + (j - 1)*imax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + 1
                        quadElem(3, elemIndex) = firstNodeIndex + 1 + imax
                        quadElem(4, elemIndex) = firstNodeIndex + imax

                    end do
                end do
                nElemEnd = elemIndex
                nBdyElem = 0
                call cg_section_write_f(index_file, index_base, index_zone, 'QuadElements', QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            end if

        end subroutine writeCgnsVolumeElements

        subroutine writeCgnsBoundaryLowI
            !----
            ! low i boundary
            ! -- WakeLow if O-type
            ! -- CFarfieldLow if C-type
            !----
            if (opt%topology.eq.'OGRD') bndName = 'WakeLow'
            if (opt%topology.eq.'CGRD') bndName = 'CFarfieldLow'

            if (threed) then

                nQuadsLoc = (jmax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = 1
                do k = 1, kmax - 1
                    do j = 1, jmax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + imax
                        quadElem(4, elemIndex) = firstNodeIndex + imax

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = (jmax - 1)
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = 1
                do j = 1, jmax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax
                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + imax

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this boundary
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowI

        subroutine writeCgnsBoundaryHighI
            !----
            ! ihi boundary
            ! -- WakeHigh if O-type
            ! -- CFarfieldHigh if C-type
            !----
            if (opt%topology.eq.'OGRD') bndName = 'WakeHigh'
            if (opt%topology.eq.'CGRD') bndName = 'CFarfieldHigh'

            if (threed) then

                nQuadsLoc = (jmax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = imax - 1
                do k = 1, kmax - 1
                    do j = 1, jmax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex + 1
                        quadElem(2, elemIndex) = firstNodeIndex + 1 + imax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1 + imax
                        quadElem(4, elemIndex) = firstNodeIndex + imax*jmax + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = (jmax - 1)
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = imax - 1
                do j = 1, jmax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax
                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + imax

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this boundary
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighI

        subroutine writeCgnsBoundaryLowJOType
            !----
            ! jlo boundary
            ! -- Airfoil if O-type
            !----
            bndName = 'Airfoil'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = 1, imax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = 1, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowJOType

        subroutine writeCgnsBoundaryLowJCType
            !----
            ! jlo boundary
            ! -- the boundary has wake and airfoil regions that
            ! need to be separated
            !----
            iLowAirfoil = opt%nwake/2**lvl + 1
            iHighAirfoil = imax - opt%nwake/2**lvl

            ! wakeLow part
            bndName = 'WakeLow'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = 1, iLowAirfoil - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = 1, iLowAirfoil - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

            ! airfoil part
            bndName = 'Airfoil'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = iLowAirfoil, iHighAirfoil - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = iLowAirfoil, iHighAirfoil - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

            ! wakeHigh part
            bndName = 'WakeHigh'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = iHighAirfoil, imax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = iHighAirfoil, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowJCType

        subroutine writeCgnsBoundaryHighJ
            !----
            ! high J boundary
            ! -- always farfield
            !----
            bndName = 'Farfield'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = jmax
                do k = 1, kmax - 1
                    do i = 1, imax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = jmax
                do i = 1, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighJ

        Subroutine writeCgnsBoundaryLowK
            !----
            ! klo boundary
            ! -- SideLow
            !----
            bndName = 'LateralLow'

            nQuadsLoc = (imax - 1)*(jmax - 1)
            allocate (quadElem(4, nQuadsLoc))

            elemIndex = 0
            nElemStart = nElemEnd + 1

            k = 1
            do j = 1, jmax - 1
                do i = 1, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                    quadElem(1, elemIndex) = firstNodeIndex
                    quadElem(2, elemIndex) = firstNodeIndex + 1
                    quadElem(3, elemIndex) = firstNodeIndex + 1 + imax
                    quadElem(4, elemIndex) = firstNodeIndex + imax

                end do
            end do

            nElemEnd = nElemStart + elemIndex - 1
            nBdyElem = 0

            call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

            deallocate (quadElem)

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowK

        subroutine writeCgnsBoundaryHighK
            !----
            ! khi boundary
            ! -- SideHigh
            !----
            bndName = 'LateralHigh'

            nQuadsLoc = (imax - 1)*(jmax - 1)
            allocate (quadElem(4, nQuadsLoc))

            elemIndex = 0
            nElemStart = nElemEnd + 1

            k = kmax - 1
            do j = 1, jmax - 1
                do i = 1, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                    quadElem(1, elemIndex) = firstNodeIndex + imax*jmax
                    quadElem(2, elemIndex) = firstNodeIndex + imax*jmax + imax
                    quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1 + imax
                    quadElem(4, elemIndex) = firstNodeIndex + imax*jmax + 1

                end do
            end do

            nElemEnd = nElemStart + elemIndex - 1
            nBdyElem = 0

            call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

            deallocate (quadElem)

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighK

    end subroutine writeGridCgnsUnstructured

    subroutine writeGridCgnsStructured(grid, opt)

        use cgns
        Use vardef, only: srf_grid_type, options_type

        type(srf_grid_type), intent(in) :: grid
        type(options_type), intent(in) :: opt

        integer imax, jmax, kmax
        integer imaxTotal, jmaxTotal, kmaxTotal
        integer i, j, k, iReverse
        integer iLowAirfoil, iHighAirfoil
        integer step
        integer icelldim, iphysdim
        integer lvl
        logical threed

        double precision, allocatable :: x_out_3d(:, :, :), y_out_3d(:, :, :), z_out_3d(:, :, :)
        double precision, allocatable :: x_out_2d(:, :), y_out_2d(:, :)
        double precision :: angle, deltaX
        double precision :: term1, term2, term, zCurrent

        integer index_file
        integer index_base, index_zone, index_coord, index_bc
        integer index_family, index_familybc
        integer ier

        integer(cgsize_t) isize_3d(3, 3), ipnts_3d(3, 2)
        integer(cgsize_t) isize_2d(2, 3), ipnts_2d(2, 2)

        character basename*32, zonename*32
        character filename*64
        character bndName*32
        character levelstr*6

        threed = .false.
        if (opt%griddim==3 .and. opt%nplanes>1) threed = .true.

        imaxTotal = grid%imax
        jmaxTotal = grid%jmax
        if (threed) then
            kmaxTotal = opt%nplanes
        else
            kmaxTotal = 1
        end if

        if (opt%multiGridLevels.gt.0) then
            if (mod(imaxTotal - 1, 2**opt%multiGridLevels).ne.0) then
                write (*, *) 'Grid (imax-1) cannot be divided by multigrid levels.'
                stop
            elseif (mod(jmaxTotal - 1, 2**opt%multiGridLevels).ne.0) then
                write (*, *) 'Grid (jmax-1) cannot be divided by multigrid levels.'
                stop
            elseif ((mod(opt%nplanes - 1, 2**opt%multiGridLevels).ne.0) .and. (threed)) then
                write (*, *) 'Grid (npln-1) cannot be divided by multigrid levels.'
                stop
            end if
        end if

        if (opt%spanwiseMethod.eq.'CLRD') then
            if (mod(opt%nplanes - 1, 2).ne.0) then
                write (*, *) 'Grid (npln-1) cannot be divided by 2. Clustered meshing will not work correctly.'
                stop
            end if
        end if

        do lvl = 0, opt%multiGridLevels

            !----
            ! set output filename
            !----
            if (opt%multiGridLevels.gt.0) then
                write (*, *) 'Multigrid Level: ', lvl
                write (levelstr, '(a5,i1)') 'Level', lvl
                filename = trim(opt%projectName)//trim(levelstr)//'.cgns'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            else
                filename = trim(opt%projectName)//'.cgns'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            end if
            !----
            ! define i-j-k bounds
            !----
            imax = (imaxTotal - 1)/2**lvl + 1
            jmax = (jmaxTotal - 1)/2**lvl + 1
            if (threed) then
                kmax = (opt%nplanes - 1)/2**lvl + 1
            else
                kmax = 1
            end if
            !----
            ! open CGNS file
            !----
            call cg_open_f(trim(filename), CG_MODE_WRITE, index_file, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f
            !----
            ! write CGNS Base node
            !----
            call writeCgnsBase
            !----
            ! write CGNS Zone node
            !----
            call writeCgnsZone
            !----
            ! write node coordinates
            !----
            call writeCgnsNodeCoor
            !----
            ! write boundary conditions
            ! -- based on O-type and C-type, the mesh boundaries have different physical meaning
            !----
            call writeCgnsBoundaryLowI
            call writeCgnsBoundaryHighI
            if (opt%topology.eq.'OGRD') call writeCgnsBoundaryLowJOType
            if (opt%topology.eq.'CGRD') call writeCgnsBoundaryLowJCType
            call writeCgnsBoundaryHighJ
            if (threed) then
                call writeCgnsBoundaryLowK
                call writeCgnsBoundaryHighK
            end if
            !----
            ! close CGNS file
            !----
            call cg_close_f(index_file, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end do

    contains

        Subroutine writeCgnsBase

            basename = 'Base'
            if (threed) then
                icelldim = 3
                iphysdim = 3
            else
                icelldim = 2
                iphysdim = 2
            end if
            call cg_base_write_f(index_file, basename, icelldim, iphysdim, index_base, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end subroutine writeCgnsBase

        subroutine writeCgnsZone

            zonename = 'Zone'

            if (threed) then
                ! nodes
                isize_3d(1, 1) = imax
                isize_3d(2, 1) = jmax
                isize_3d(3, 1) = kmax
                ! cells
                isize_3d(1, 2) = imax - 1
                isize_3d(2, 2) = jmax - 1
                isize_3d(3, 2) = kmax - 1
                ! boundary
                isize_3d(1, 3) = 0
                isize_3d(2, 3) = 0
                isize_3d(3, 3) = 0
            else
                ! nodes
                isize_2d(1, 1) = imax
                isize_2d(2, 1) = jmax
                ! cells
                isize_2d(1, 2) = imax - 1
                isize_2d(2, 2) = jmax - 1
                ! boundary
                isize_2d(1, 3) = 0
                isize_2d(2, 3) = 0
            end if

        end subroutine writeCgnsZone

        subroutine writeCgnsNodeCoor

            if (threed) then
                allocate (x_out_3d(imax, jmax, kmax), y_out_3d(imax, jmax, kmax), z_out_3d(imax, jmax, kmax))
            else
                allocate (x_out_2d(imax, jmax), y_out_2d(imax, jmax))
            end if

            step = 2**(lvl)

            do k = 1, kmax

                if (opt%spanwiseMethod.eq.'UNFM') then
                    zCurrent = dble((k - 1)*step)*opt%spanwiseSpacing
                else
                    if (k.eq.0) then
                        zCurrent = 0.d0
                    else
                        term1 = tanh(opt%zeta*(2.d0*k/(kmax - 1) - 1.d0))
                        term2 = tanh(opt%zeta)
                        term = (1.d0 + term1/term2)
                        zCurrent = (opt%span/2.d0)*term
                    end if
                end if

                if (opt%alphaSweep.gt.0.d0) then
                    angle = opt%alphaSweep*4.d0*atan(1.d0)/180.d0
                    deltaX = tan(angle)*zCurrent
                else
                    deltaX = 0.d0
                end if

                do j = 1, jmax
                    do i = 1, imax
                        iReverse = imax + 1 - i ! x and y are in reverse i order to preserve positive volumes
                        if (threed) then
                            x_out_3d(i, j, k) = grid%x((iReverse - 1)*step + 1, (j - 1)*step + 1) + deltaX
                            y_out_3d(i, j, k) = grid%y((iReverse - 1)*step + 1, (j - 1)*step + 1)
                            z_out_3d(i, j, k) = zCurrent
                        else
                            x_out_2d(i, j) = grid%x((iReverse - 1)*step + 1, (j - 1)*step + 1)
                            y_out_2d(i, j) = grid%y((iReverse - 1)*step + 1, (j - 1)*step + 1)
                        end if
                    end do
                end do

            end do

            if (threed) then

                call cg_zone_write_f(index_file, index_base, zonename, isize_3d, Structured, index_zone, ier)
                if (ier.ne.CG_OK) call cg_error_exit_f

                call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateX', x_out_3d, index_coord, ier)
                call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateY', y_out_3d, index_coord, ier)
                call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateZ', z_out_3d, index_coord, ier)

            else

                call cg_zone_write_f(index_file, index_base, zonename, isize_2d, Structured, index_zone, ier)
                if (ier.ne.CG_OK) call cg_error_exit_f
                call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateX', x_out_2d, index_coord, ier)
                call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateY', y_out_2d, index_coord, ier)

            end if

            if (threed) then
                deallocate (x_out_3d, y_out_3d, z_out_3d)
            else
                deallocate (x_out_2d, y_out_2d)
            end if

        end subroutine writeCgnsNodeCoor

        subroutine writeCgnsBoundaryLowI
            !----
            ! low i boundary
            ! -- WakeLow if O-type
            ! -- CFarfieldLow if C-type
            !----
            if (opt%topology.eq.'OGRD') bndName = 'WakeLow'
            if (opt%topology.eq.'CGRD') bndName = 'CFarfieldLow'

            !----
            ! create family node for this boundary
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! set boundary i-j-k bounds and GridLocation
            !----
            if (threed) then

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_3d(1, 1) = 1
                ipnts_3d(2, 1) = 1
                ipnts_3d(3, 1) = 1
                ! higher point of range
                ipnts_3d(1, 2) = 1
                ipnts_3d(2, 2) = jmax
                ipnts_3d(3, 2) = kmax

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            else

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_2d(1, 1) = 1
                ipnts_2d(2, 1) = 1
                ! higher point of range
                ipnts_2d(1, 2) = 1
                ipnts_2d(2, 2) = jmax

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            end if

            !----
            ! write nested family node
            !----
            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowI

        subroutine writeCgnsBoundaryHighI
            !----
            ! ihi boundary
            ! -- WakeHigh if O-type
            ! -- CFarfieldHigh if C-type
            !----
            if (opt%topology.eq.'OGRD') bndName = 'WakeHigh'
            if (opt%topology.eq.'CGRD') bndName = 'CFarfieldHigh'

            !----
            ! create family node for this boundary
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! set boundary i-j-k bounds and write location
            !----
            if (threed) then

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_3d(1, 1) = imax
                ipnts_3d(2, 1) = 1
                ipnts_3d(3, 1) = 1
                ! higher point of range
                ipnts_3d(1, 2) = imax
                ipnts_3d(2, 2) = jmax
                ipnts_3d(3, 2) = kmax

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            else

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_2d(1, 1) = imax
                ipnts_2d(2, 1) = 1
                ! higher point of range
                ipnts_2d(1, 2) = imax
                ipnts_2d(2, 2) = jmax

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            end if

            !----
            ! write nested family node
            !----
            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighI

        subroutine writeCgnsBoundaryLowJOType
            !----
            ! jlo boundary
            ! -- Airfoil if O-type
            !----
            bndName = 'Airfoil'

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            if (threed) then

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_3d(1, 1) = 1
                ipnts_3d(2, 1) = 1
                ipnts_3d(3, 1) = 1
                ! higher point of range
                ipnts_3d(1, 2) = imax
                ipnts_3d(2, 2) = 1
                ipnts_3d(3, 2) = kmax

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            else

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_2d(1, 1) = 1
                ipnts_2d(2, 1) = 1
                ! higher point of range
                ipnts_2d(1, 2) = imax
                ipnts_2d(2, 2) = 1

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            end if

            !----
            ! write nested family node
            !----
            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowJOType

        subroutine writeCgnsBoundaryLowJCType
            !----
            ! jlo boundary
            ! -- the boundary has both wake and airfoil regions that need to be separated
            !----
            iLowAirfoil = opt%nwake/2**lvl + 1
            iHighAirfoil = imax - opt%nwake/2**lvl

            !----
            ! wakeLow part
            !----
            bndName = 'WakeLow'

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            if (threed) then

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_3d(1, 1) = 1
                ipnts_3d(2, 1) = 1
                ipnts_3d(3, 1) = 1
                ! higher point of range
                ipnts_3d(1, 2) = iLowAirfoil
                ipnts_3d(2, 2) = 1
                ipnts_3d(3, 2) = kmax

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            else

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_2d(1, 1) = 1
                ipnts_2d(2, 1) = 1
                ! higher point of range
                ipnts_2d(1, 2) = iLowAirfoil
                ipnts_2d(2, 2) = 1

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            end if

            !----
            ! write nested family node
            !----
            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

            !----
            ! airfoil part
            !----
            bndName = 'Airfoil'

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            if (threed) then

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_3d(1, 1) = iLowAirfoil
                ipnts_3d(2, 1) = 1
                ipnts_3d(3, 1) = 1
                ! higher point of range
                ipnts_3d(1, 2) = iHighAirfoil
                ipnts_3d(2, 2) = 1
                ipnts_3d(3, 2) = kmax

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            else

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_2d(1, 1) = iLowAirfoil
                ipnts_2d(2, 1) = 1
                ! higher point of range
                ipnts_2d(1, 2) = iHighAirfoil
                ipnts_2d(2, 2) = 1

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            end if

            !----
            ! write nested family node
            !----
            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

            !----
            ! wakeHigh part
            !----
            bndName = 'WakeHigh'

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            if (threed) then

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_3d(1, 1) = iHighAirfoil
                ipnts_3d(2, 1) = 1
                ipnts_3d(3, 1) = 1
                ! higher point of range
                ipnts_3d(1, 2) = imax
                ipnts_3d(2, 2) = 1
                ipnts_3d(3, 2) = kmax

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            else

                !----
                ! BC point ranges
                !----
                ! lower point of range
                ipnts_2d(1, 1) = iHighAirfoil
                ipnts_2d(2, 1) = 1
                ! higher point of range
                ipnts_2d(1, 2) = imax
                ipnts_2d(2, 2) = 1

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            end if

            !----
            ! write nested family node
            !----
            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowJCType

        subroutine writeCgnsBoundaryHighJ
            !----
            ! high J boundary
            ! -- always farfield
            !----
            bndName = 'Farfield'

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            if (threed) then

                ! lower point of range
                ipnts_3d(1, 1) = 1
                ipnts_3d(2, 1) = jmax
                ipnts_3d(3, 1) = 1
                ! higher point of range
                ipnts_3d(1, 2) = imax
                ipnts_3d(2, 2) = jmax
                ipnts_3d(3, 2) = kmax

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            else

                ! lower point of range
                ipnts_2d(1, 1) = 1
                ipnts_2d(2, 1) = jmax
                ! higher point of range
                ipnts_2d(1, 2) = imax
                ipnts_2d(2, 2) = jmax

                !----
                ! write BC points
                !----
                call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                !----
                ! write BClocation
                !----
                call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            end if

            !----
            ! write nested family node
            !----
            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighJ

        subroutine writeCgnsBoundaryLowK
            !----
            ! klo boundary
            ! -- SideLow
            !----
            bndName = 'LateralLow'

            ! lower point of range
            ipnts_3d(1, 1) = 1
            ipnts_3d(2, 1) = 1
            ipnts_3d(3, 1) = 1
            ! higher point of range
            ipnts_3d(1, 2) = imax
            ipnts_3d(2, 2) = jmax
            ipnts_3d(3, 2) = 1

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
            !----
            ! write BClocation
            !----
            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            !----
            ! write nested family node
            !----
            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowK

        subroutine writeCgnsBoundaryHighK
            !----
            ! khi boundary -- Side
            !----
            bndName = 'LateralHigh'

            ! lower point of range
            ipnts_3d(1, 1) = 1
            ipnts_3d(2, 1) = 1
            ipnts_3d(3, 1) = kmax
            ! higher point of range
            ipnts_3d(1, 2) = imax
            ipnts_3d(2, 2) = jmax
            ipnts_3d(3, 2) = kmax

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
            !----
            ! write BClocation
            !----
            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            !----
            ! write nested family node
            !----
            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighK

    end subroutine writeGridCgnsStructured

    subroutine writeGridCgnsUnstructuredWithoutWakeLine(grid, opt)

        use cgns
        Use vardef, only: srf_grid_type, options_type

        type(srf_grid_type), intent(in) :: grid
        type(options_type), intent(in) :: opt

        integer ier

        integer imax, jmax, kmax
        integer imaxTotal, jmaxTotal, kmaxTotal
        integer i, j, k, iReverse
        integer iLowAirfoil, iHighAirfoil
        integer step
        integer icelldim, iphysdim
        integer lvl
        integer nodeIndex
        logical threed

        integer(cgsize_t) isize(1, 3)

        !----
        ! CGNS indices
        !----
        integer index_file
        integer index_base, index_zone, index_coord, index_section, index_bc
        integer index_family, index_familybc

        !----
        ! nodes
        !----
        double precision, allocatable :: x_out(:), y_out(:), z_out(:)
        double precision :: angle, deltaX

        !----
        ! elements
        !----
        integer(CGSIZE_T), allocatable :: hexaElem(:, :)
        integer(CGSIZE_T), allocatable :: quadElem(:, :)
        integer(CGSIZE_T), allocatable :: barElem(:, :)
        integer :: elemIndex
        integer :: firstNodeIndex
        integer(CGSIZE_T) :: nElemStart, nElemEnd
        integer :: nBdyElem
        integer :: nQuadsLoc
        integer :: nBarsLoc

        !----
        ! boundary conditions
        !----
        integer(CGSIZE_T), allocatable :: bcPnts(:)
        integer(CGSIZE_T) :: bcCount

        character basename*32, zonename*32
        character bndName*32
        character filename*64
        character levelstr*6

        threed = .false.
        if (opt%griddim==3 .and. opt%nplanes>1) threed = .true.

        if (opt%topology.eq.'OGRD') then
            imaxTotal = grid%imax - 1
            jmaxTotal = grid%jmax
            if (threed) then
                kmaxTotal = opt%nplanes
            else
                kmaxTotal = 1
            end if
        elseif (opt%topology.eq.'CGRD') then
            stop 'C-type not supported at the moment for CGNS without wake line'
        end if

        if (opt%multiGridLevels.gt.0) then
            if (mod(imaxTotal - 1, 2**opt%multiGridLevels).ne.0) then
                write (*, *) 'Grid (imax-1) cannot be divided by multigrid levels.'
                stop
            elseif (mod(jmaxTotal - 1, 2**opt%multiGridLevels).ne.0) then
                write (*, *) 'Grid (jmax-1) cannot be divided by multigrid levels.'
                stop
            elseif ((mod(opt%nplanes - 1, 2**opt%multiGridLevels).ne.0) .and. (threed)) then
                write (*, *) 'Grid (npln-1) cannot be divided by multigrid levels.'
                stop
            end if
        end if

        if (opt%spanwiseMethod.eq.'CLRD') then
            if (mod(opt%nplanes - 1, 2).ne.0) then
                write (*, *) 'Grid (npln-1) cannot be divided by 2. Clustered meshing will not work correctly.'
                stop
            end if
        end if

        do lvl = 0, opt%multiGridLevels

            !----
            ! set output filename
            !----
            if (opt%multiGridLevels.gt.0) then
                write (*, *) 'Multigrid Level: ', lvl
                write (levelstr, '(a5,i1)') 'Level', lvl
                filename = trim(opt%projectName)//trim(levelstr)//'.cgns'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            else
                filename = trim(opt%projectName)//'.cgns'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            end if
            !----
            ! define i-j-k bounds
            !----
            imax = (imaxTotal - 1)/2**lvl + 1
            jmax = (jmaxTotal - 1)/2**lvl + 1
            if (threed) then
                kmax = (opt%nplanes - 1)/2**lvl + 1
            else
                kmax = 1
            end if
            !----
            ! open CGNS file
            !----
            call cg_open_f(trim(filename), CG_MODE_WRITE, index_file, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f
            !----
            ! write CGNS Base node
            !----
            call writeCgnsBase
            !----
            ! write CGNS Zone node
            !----
            call writeCgnsZone
            !----
            ! write node coordinates
            !----
            call writeCgnsNodeCoor
            !----
            ! write volume elements
            !----
            call writeCgnsVolumeElements
            !----
            ! write boundary element sections and boundary conditions
            ! -- based on O-type and C-type, the mesh boundaries have different physical meaning
            !----
            ! call writeCgnsBoundaryLowI
            ! call writeCgnsBoundaryHighI
            if (opt%topology.eq.'OGRD') call writeCgnsBoundaryLowJOType
            if (opt%topology.eq.'CGRD') call writeCgnsBoundaryLowJCType
            call writeCgnsBoundaryHighJ
            if (threed) then
                call writeCgnsBoundaryLowK
                call writeCgnsBoundaryHighK
            end if
            !----
            ! close CGNS file
            !----
            call cg_close_f(index_file, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end do

    contains

        Subroutine writeCgnsBase
            !----
            ! base
            !----
            basename = 'Base'
            if (threed) then
                icelldim = 3
                iphysdim = 3
            else
                icelldim = 2
                iphysdim = 2
            end if
            call cg_base_write_f(index_file, basename, icelldim, iphysdim, index_base, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end subroutine writeCgnsBase

        subroutine writeCgnsZone

            zonename = 'Zone'

            ! nodes
            isize(1, 1) = imax*jmax*kmax
            ! cells
            if (threed) isize(1, 2) = imax*(jmax - 1)*(kmax - 1)
            if (.not. threed) isize(1, 2) = imax*(jmax - 1)*kmax
            ! boundary
            isize(1, 3) = 0

            call cg_zone_write_f(index_file, index_base, zonename, isize, Unstructured, index_zone, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end subroutine writeCgnsZone

        subroutine writeCgnsNodeCoor

            double precision :: term1, term2, term, zCurrent

            allocate (x_out(imax*jmax*kmax), y_out(imax*jmax*kmax), z_out(imax*jmax*kmax))

            step = 2**(lvl)

            nodeIndex = 0
            do k = 1, kmax

                if (opt%spanwiseMethod.eq.'UNFM') then
                    zCurrent = dble((k - 1)*step)*opt%spanwiseSpacing
                else
                    if (k.eq.0) then
                        zCurrent = 0.d0
                    else
                        term1 = tanh(opt%zeta*(2.d0*k/(kmax - 1) - 1.d0))
                        term2 = tanh(opt%zeta)
                        term = (1.d0 + term1/term2)
                        zCurrent = (opt%span/2.d0)*term
                    end if
                end if

                if (opt%alphaSweep.gt.0.d0) then
                    angle = opt%alphaSweep*4.d0*atan(1.d0)/180.d0
                    deltaX = tan(angle)*zCurrent
                else
                    deltaX = 0.d0
                end if

                do j = 1, jmax
                    do i = 1, imax
                        iReverse = imax + 1 - i ! x and y are in reverse i order to preserve positive volumes
                        nodeIndex = nodeIndex + 1
                        if (threed) then
                            x_out(nodeIndex) = grid%x((iReverse - 1)*step + 1, (j - 1)*step + 1) + deltaX
                            y_out(nodeIndex) = grid%y((iReverse - 1)*step + 1, (j - 1)*step + 1)
                            z_out(nodeIndex) = zCurrent
                        else
                            x_out(nodeIndex) = grid%x((iReverse - 1)*step + 1, (j - 1)*step + 1)
                            y_out(nodeIndex) = grid%y((iReverse - 1)*step + 1, (j - 1)*step + 1)
                            z_out(nodeIndex) = 0.d0
                        end if
                    end do
                end do

            end do

            call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateX', x_out, index_coord, ier)
            call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateY', y_out, index_coord, ier)
            if (threed) call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateZ', z_out, index_coord, ier)

            deallocate (x_out, y_out, z_out)

        end subroutine writeCgnsNodeCoor

        subroutine writeCgnsVolumeElements

            integer :: renumberOffset

            if (threed) then

                !----
                ! hexas
                !----
                allocate (hexaElem(8, (imax)*(jmax)*(kmax - 1)))

                nElemStart = 1
                elemIndex = 0
                do k = 1, kmax - 1
                    do j = 1, jmax - 1
                        do i = 1, imax

                            elemIndex = elemIndex + 1

                            firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                            hexaElem(1, elemIndex) = firstNodeIndex
                            hexaElem(2, elemIndex) = firstNodeIndex + 1
                            if (i.eq.imax) hexaElem(2, elemIndex) = 1 + (j - 1)*imax + (k - 1)*imax*jmax
                            hexaElem(3, elemIndex) = firstNodeIndex + 1 + imax
                            if (i.eq.imax) hexaElem(3, elemIndex) = 1 + (j)*imax + (k - 1)*imax*jmax
                            hexaElem(4, elemIndex) = firstNodeIndex + imax

                            hexaElem(5, elemIndex) = firstNodeIndex + imax*jmax
                            hexaElem(6, elemIndex) = firstNodeIndex + imax*jmax + 1
                            if (i.eq.imax) hexaElem(6, elemIndex) = 1 + (j - 1)*imax + (k - 1)*imax*jmax + imax*jmax
                            hexaElem(7, elemIndex) = firstNodeIndex + imax*jmax + 1 + imax
                            if (i.eq.imax) hexaElem(7, elemIndex) = 1 + (j)*imax + (k - 1)*imax*jmax + imax*jmax
                            hexaElem(8, elemIndex) = firstNodeIndex + imax*jmax + imax

                        end do
                    end do
                end do
                nElemEnd = elemIndex
                nBdyElem = 0
                call cg_section_write_f(index_file, index_base, index_zone, 'HexaElements', HEXA_8, nElemStart, nElemEnd, nBdyElem, hexaElem, index_section, ier)

                deallocate (hexaElem)

            else

                !----
                ! hexas
                !----
                allocate (quadElem(4, (imax)*(jmax)))

                nElemStart = 1
                elemIndex = 0
                do j = 1, jmax - 1
                    do i = 1, imax

                        elemIndex = elemIndex + 1

                        firstNodeIndex = i + (j - 1)*imax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + 1
                        if (i.eq.imax) quadElem(2, elemIndex) = 1 + (j - 1)*imax
                        quadElem(3, elemIndex) = firstNodeIndex + 1 + imax
                        if (i.eq.imax) quadElem(3, elemIndex) = 1 + (j)*imax
                        quadElem(4, elemIndex) = firstNodeIndex + imax

                    end do
                end do
                nElemEnd = elemIndex
                nBdyElem = 0
                call cg_section_write_f(index_file, index_base, index_zone, 'QuadElements', QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            end if

        end subroutine writeCgnsVolumeElements

        subroutine writeCgnsBoundaryLowI
            !----
            ! low i boundary
            ! -- WakeLow if O-type
            ! -- CFarfieldLow if C-type
            !----
            if (opt%topology.eq.'OGRD') bndName = 'WakeLow'
            if (opt%topology.eq.'CGRD') bndName = 'CFarfieldLow'

            if (threed) then

                nQuadsLoc = (jmax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = 1
                do k = 1, kmax - 1
                    do j = 1, jmax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + imax
                        quadElem(4, elemIndex) = firstNodeIndex + imax

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = (jmax - 1)
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = 1
                do j = 1, jmax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax
                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + imax

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this boundary
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowI

        subroutine writeCgnsBoundaryHighI
            !----
            ! ihi boundary
            ! -- WakeHigh if O-type
            ! -- CFarfieldHigh if C-type
            !----
            if (opt%topology.eq.'OGRD') bndName = 'WakeHigh'
            if (opt%topology.eq.'CGRD') bndName = 'CFarfieldHigh'

            if (threed) then

                nQuadsLoc = (jmax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = imax - 1
                do k = 1, kmax - 1
                    do j = 1, jmax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex + 1
                        quadElem(2, elemIndex) = firstNodeIndex + 1 + imax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1 + imax
                        quadElem(4, elemIndex) = firstNodeIndex + imax*jmax + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = (jmax - 1)
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = imax - 1
                do j = 1, jmax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax
                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + imax

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this boundary
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighI

        subroutine writeCgnsBoundaryLowJOType
            !----
            ! jlo boundary
            ! -- Airfoil if O-type
            !----
            bndName = 'Airfoil'

            if (threed) then

                nQuadsLoc = (imax)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = 1, imax

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        if (i.eq.imax) quadElem(3, elemIndex) = 1 + (j - 1)*imax + (k - 1)*imax*jmax + imax*jmax
                        quadElem(4, elemIndex) = firstNodeIndex + 1
                        if (i.eq.imax) quadElem(4, elemIndex) = 1 + (j - 1)*imax + (k - 1)*imax*jmax

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = 1, imax

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1
                    if (i.eq.imax) barElem(2, elemIndex) = 1 + (j - 1)*imax

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowJOType

        subroutine writeCgnsBoundaryLowJCType
            !----
            ! jlo boundary
            ! -- the boundary has wake and airfoil regions that
            ! need to be separated
            !----
            iLowAirfoil = opt%nwake/2**lvl + 1
            iHighAirfoil = imax - opt%nwake/2**lvl

            ! wakeLow part
            bndName = 'WakeLow'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = 1, iLowAirfoil - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = 1, iLowAirfoil - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

            ! airfoil part
            bndName = 'Airfoil'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = iLowAirfoil, iHighAirfoil - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = iLowAirfoil, iHighAirfoil - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

            ! wakeHigh part
            bndName = 'WakeHigh'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = iHighAirfoil, imax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = iHighAirfoil, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowJCType

        subroutine writeCgnsBoundaryHighJ
            !----
            ! high J boundary
            ! -- always farfield
            !----
            bndName = 'Farfield'

            if (threed) then

                nQuadsLoc = (imax)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = jmax
                do k = 1, kmax - 1
                    do i = 1, imax

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        if (i.eq.imax) quadElem(3, elemIndex) = 1 + (j - 1)*imax + (k - 1)*imax*jmax + imax*jmax
                        quadElem(4, elemIndex) = firstNodeIndex + 1
                        if (i.eq.imax) quadElem(4, elemIndex) = 1 + (j - 1)*imax + (k - 1)*imax*jmax

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = jmax
                do i = 1, imax

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1
                    if (i.eq.imax) barElem(2, elemIndex) = 1 + (j - 1)*imax

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighJ

        Subroutine writeCgnsBoundaryLowK
            !----
            ! klo boundary
            ! -- SideLow
            !----
            bndName = 'LateralLow'

            nQuadsLoc = (imax)*(jmax - 1)
            allocate (quadElem(4, nQuadsLoc))

            elemIndex = 0
            nElemStart = nElemEnd + 1

            k = 1
            do j = 1, jmax - 1
                do i = 1, imax

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                    quadElem(1, elemIndex) = firstNodeIndex
                    quadElem(2, elemIndex) = firstNodeIndex + 1
                    if (i.eq.imax) quadElem(2, elemIndex) = 1 + (j - 1)*imax + (k - 1)*imax*jmax
                    quadElem(3, elemIndex) = firstNodeIndex + 1 + imax
                    if (i.eq.imax) quadElem(3, elemIndex) = 1 + (j - 1)*imax + (k - 1)*imax*jmax + imax
                    quadElem(4, elemIndex) = firstNodeIndex + imax

                end do
            end do

            nElemEnd = nElemStart + elemIndex - 1
            nBdyElem = 0

            call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

            deallocate (quadElem)

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowK

        subroutine writeCgnsBoundaryHighK
            !----
            ! khi boundary
            ! -- SideHigh
            !----
            bndName = 'LateralHigh'

            nQuadsLoc = (imax)*(jmax - 1)
            allocate (quadElem(4, nQuadsLoc))

            elemIndex = 0
            nElemStart = nElemEnd + 1

            k = kmax - 1
            do j = 1, jmax - 1
                do i = 1, imax

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                    quadElem(1, elemIndex) = firstNodeIndex + imax*jmax
                    quadElem(2, elemIndex) = firstNodeIndex + imax*jmax + imax
                    quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1 + imax
                    if (i.eq.imax) quadElem(3, elemIndex) = 1 + (j - 1)*imax + (k - 1)*imax*jmax + imax*jmax + imax
                    quadElem(4, elemIndex) = firstNodeIndex + imax*jmax + 1
                    if (i.eq.imax) quadElem(4, elemIndex) = 1 + (j - 1)*imax + (k - 1)*imax*jmax + imax*jmax

                end do
            end do

            nElemEnd = nElemStart + elemIndex - 1
            nBdyElem = 0

            call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

            deallocate (quadElem)

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighK

    end subroutine writeGridCgnsUnstructuredWithoutWakeLine

end module cgnsOutput
