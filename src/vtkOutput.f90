!  This file is part of structAirfoilMesher.

!  structAirfoilMesher is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.

!  structAirfoilMesher is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.

!  You should have received a copy of the GNU General Public License
!  along with structAirfoilMesher.  If not, see <http://www.gnu.org/licenses/>.

!  author: Konstantinos Diakakis

module vtkOutput

    implicit none

contains

    subroutine writeGridVtkStructured(grid, opt)

        Use vardef, only: srf_grid_type, options_type

        type(srf_grid_type), intent(in) :: grid
        type(options_type), intent(in) :: opt

        integer i, j, k
        integer imaxTotal, jmaxTotal, kmaxTotal
        integer imax, jmax, kmax
        integer lvl, step
        logical threed

        double precision :: angle, deltaX
        double precision :: term1, term2, term, zCurrent

        character filename*64, filename2*64
        character levelstr*6

        threed = .false.
        if (opt%griddim==3 .and. opt%nplanes>1) threed = .true.

        imaxTotal = grid%imax
        jmaxTotal = grid%jmax
        if (threed) then
            kmaxTotal = opt%nplanes
        else
            kmaxTotal = 1
        end if

        if (opt%multiGridLevels.gt.0) then
            if (mod(imaxTotal - 1, 2**opt%multiGridLevels).ne.0) then
                write (*, *) 'Grid (imax-1) cannot be divided by multigrid levels.'
                stop
            elseif (mod(jmaxTotal - 1, 2**opt%multiGridLevels).ne.0) then
                write (*, *) 'Grid (jmax-1) cannot be divided by multigrid levels.'
                stop
            elseif ((mod(opt%nplanes - 1, 2**opt%multiGridLevels).ne.0) .and. (threed)) then
                write (*, *) 'Grid (npln-1) cannot be divided by multigrid levels.'
                stop
            end if
        end if

        if (opt%spanwiseMethod.eq.'CLRD') then
            if (mod(opt%nplanes-1, 2).ne.0) then
                write (*, *) 'Grid (npln-1) cannot be divided by 2. Clustered meshing will not work correctly.'
                stop
            endif
        endif

        do lvl = 0, opt%multiGridLevels

            if (opt%multiGridLevels.gt.0) then
                write (*, *) 'Multigrid Level: ', lvl
                write (levelstr, '(a5,i1)') 'Level', lvl
                filename = trim(opt%projectName)//trim(levelstr)//'.vtk'
                if (.not.threed) filename2 = trim(opt%projectName)//trim(levelstr)//'With0ZDimension.vtk'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            else
                filename = trim(opt%projectName)//'.vtk'
                if (.not.threed) filename2 = trim(opt%projectName)//'With0ZDimension.vtk'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            end if

            imax = (imaxTotal - 1)/2**lvl + 1
            jmax = (jmaxTotal - 1)/2**lvl + 1
            if (threed) then
                kmax = (opt%nplanes - 1)/2**lvl + 1
            else
                kmax = 1
            end if

            open (1, file=trim(filename))
            if (.not.threed) open (2, file=trim(filename2))
            step = 2**(lvl)

            !----
            ! Write header to output file
            !----
            if (threed) then

                write (1, '("# vtk DataFile Version 2.0")')
                if (opt%multiGridLevels.gt.0) write (1, *) trim(opt%projectName)//" Structured Mesh - Multigrid "//trim(levelstr)
                if (opt%multiGridLevels.eq.0) write (1, *) trim(opt%projectName)//" Structured Mesh"
                write (1, *) 'ASCII'
                write (1, *) 'DATASET STRUCTURED_GRID'
                write (1, *) 'DIMENSIONS ', imax, jmax, kmax
                write (1, *) 'POINTS ', imax*jmax*kmax, ' Double'

            else

                ! Pure 2D VTK
                write (1, '("# vtk DataFile Version 2.0")')
                if (opt%multiGridLevels.gt.0) write (1, *) trim(opt%projectName)//" Structured Mesh - Multigrid "//trim(levelstr)
                if (opt%multiGridLevels.eq.0) write (1, *) trim(opt%projectName)//" Structured Mesh"
                write (1, *) 'ASCII'
                write (1, *) 'DATASET STRUCTURED_GRID'
                write (1, *) 'DIMENSIONS ', imax, jmax
                write (1, *) 'POINTS ', imax*jmax, ' Double'

                ! 3D VTK with 0 Z dimension
                write (2, '("# vtk DataFile Version 2.0")')
                if (opt%multiGridLevels.gt.0) write (2, *) trim(opt%projectName)//" Structured Mesh - Multigrid "//trim(levelstr)
                if (opt%multiGridLevels.eq.0) write (2, *) trim(opt%projectName)//" Structured Mesh"
                write (2, *) 'ASCII'
                write (2, *) 'DATASET STRUCTURED_GRID'
                write (2, *) 'DIMENSIONS ', imax, jmax, 1
                write (2, *) 'POINTS ', imax*jmax*1, ' Double'

            endif

            ! Write out grid - in reverse i direction to preserve positive volumes

            if (threed) then

                do k = 0, kmax-1 ! swtiching indexing from 1 to kmax to 0 to kmax-1 in order to have z=0 as the start

                    if (opt%spanwiseMethod.eq.'UNFM') then
                        zCurrent = dble(k*step)*opt%spanwiseSpacing
                    else
                        if (k.eq.0) then
                            zCurrent = 0.d0
                        else
                            term1 = tanh(opt%zeta*(2.d0*k/(kmax - 1) - 1.d0))
                            term2 = tanh(opt%zeta)
                            term = (1.d0 + term1/term2)
                            zCurrent = (opt%span/2.d0)*term
                        end if
                    end if

                    if (opt%alphaSweep.gt.0.d0) then
                        angle = opt%alphaSweep*4.d0*atan(1.d0)/180.d0
                        deltaX = tan(angle)*zCurrent
                    else
                        deltaX = 0.d0
                    end if

                    do j = 1, jmax
                        do i = imax, 1, -1
                            write (1, '(3(es17.8,1x))') grid%x((i - 1)*step + 1, (j - 1)*step + 1) + deltaX, &
                                grid%y((i - 1)*step + 1, (j - 1)*step + 1), &
                                zCurrent
                        end do
                    end do

                end do

            else

                do j = 1, jmax
                    do i = imax, 1, -1
                        write (1, '(2(es17.8,1x))') grid%x((i - 1)*step + 1, (j - 1)*step + 1), &
                            grid%y((i - 1)*step + 1, (j - 1)*step + 1)
                        write (2, '(3(es17.8,1x))') grid%x((i - 1)*step + 1, (j - 1)*step + 1), &
                            grid%y((i - 1)*step + 1, (j - 1)*step + 1), 0.d0
                    end do
                end do

            end if

            close (1)
            if (.not.threed) close (2)

        end do

    end subroutine writeGridVtkStructured

end module vtkOutput
