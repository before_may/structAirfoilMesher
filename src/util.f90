!  This file is part of structAirfoilMesher.

!  structAirfoilMesher is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.

!  structAirfoilMesher is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.

!  You should have received a copy of the GNU General Public License
!  along with structAirfoilMesher.  If not, see <http://www.gnu.org/licenses/>.

!  This file was originally part of Construct2D
!  https://sourceforge.net/projects/construct2d
!  author: Daniel Prosser
!  Copyright (C) 2013 -- 2018

!  Modifications for structAirfoilMesher
!  author: Konstantinos Diakakis 

module util

! Contains subroutines to perform utility-type operations for the grid
! generator (reading/writing files, parsing CLOs, etc)

    implicit none

contains

!=============================================================================80
!
! Prints out greeting
!
!=============================================================================80
    subroutine greeting

        write (*, *)
        write (*, *) "############################################################################################"
        write (*, *) 
        write (*, *) "     _                   _      _    _       __       _ _ __  __           _                "
        write (*, *) " ___| |_ _ __ _   _  ___| |_   / \  (_)_ __ / _| ___ (_) |  \/  | ___  ___| |__   ___ _ __  "
        write (*, *) "/ __| __| '__| | | |/ __| __| / _ \ | | '__| |_ / _ \| | | |\/| |/ _ \/ __| '_ \ / _ \ '__| "
        write (*, *) "\__ \ |_| |  | |_| | (__| |_ / ___ \| | |  |  _| (_) | | | |  | |  __/\__ \ | | |  __/ |    "
        write (*, *) "|___/\__|_|   \__,_|\___|\__/_/   \_\_|_|  |_|  \___/|_|_|_|  |_|\___||___/_| |_|\___|_|    "
        write (*, *) 
        write (*, *) 'Command line utility to create structured O-type and C-type meshes for airfoils'
        write (*, *) 
        write (*, *) 'Version: '//trim(VERSION)
        write (*, *) 'Commit: '//trim(COMMIT)
        write (*, *)
        write (*, *) 
        write (*, *) "############################################################################################"

    end subroutine greeting

!=============================================================================80
!
! Subroutine to read command line options and set airfoil file name
!
!=============================================================================80
    subroutine read_clo(filename)

        character(*), intent(inout) :: filename

        filename = ''

        call getarg(1, filename)

    end subroutine read_clo

!=============================================================================80
!
! Subroutine to read airfoil file name from user input
!
!=============================================================================80
    subroutine read_airfoil_name(filename)

        character(*), intent(inout) :: filename

        write (*, *)
        write (*, *) 'Enter name of airfoil to load (XFOIL labeled format)'
        write (*, *) ' or QUIT to close the program:'
        write (*, 1001)
        read (*, '(A)') filename

        if (trim(filename)=='QUIT' .or. trim(filename)=='Quit' .or. &
            trim(filename)=='quit') stop

1001    format(/' Input > ', $)

    end subroutine read_airfoil_name

!=============================================================================80
!
! Subroutine to read airfoil dimensions (XFoil labeled format)
!
!=============================================================================80
    subroutine read_airfoil_size(filename, dimensions, ioerror)

        character(*), intent(inout) :: filename
        integer, intent(inout) :: dimensions, ioerror

        integer iunit

! Open file

        iunit = 12
        open (iunit, file=filename, status='old', iostat=ioerror)

        if (ioerror/=0) then

            write (*, *)
            write (*, *) 'Error: airfoil file '//trim(filename)//' could not be found.'
            filename = ''

        else

!   Read number of points on surface

            dimensions = 0
            read (iunit, *)         ! Skip past header
            do
                read (iunit, *, end=500)
                dimensions = dimensions + 1
            end do

        end if

500     close (iunit)

    end subroutine read_airfoil_size

!=============================================================================80
!
! Subroutine to read airfoil data (XFoil labeled format)
!
!=============================================================================80
    subroutine read_airfoil(filename, npoints, x, y)

        character(*), intent(in) :: filename
        integer, intent(in) :: npoints
        double precision, dimension(npoints), intent(out) :: x, y

        integer i, iunit
        double precision, dimension(npoints) :: xtmp, ytmp

! Open file

        iunit = 13
        open (iunit, file=filename, status='old')

! Read data

        read (iunit, *)                     ! Skip past header
        do i = 1, npoints
            read (iunit, *) x(i), y(i)
        end do

! Flip direction if needed to ensure counterclockwise ordering

        if (y(2)<y(npoints - 1)) then

            write (*, *)
            write (*, *) 'Changing point ordering to counter-clockwise ...'
            xtmp = x
            ytmp = y
            do i = 1, npoints
                x(i) = xtmp(npoints - i + 1)
                y(i) = ytmp(npoints - i + 1)
            end do

        end if

! Close file and print message

        close (iunit)
        write (*, *)
        write (*, *) 'Successfully loaded airfoil file '//trim(filename)
        write (*, *) ' Number of points:', npoints

    end subroutine read_airfoil

!=============================================================================80
!
! Flips the order of the characters in a string
!
!=============================================================================80
    function flip_text(str) result(backstr)

        character(*), intent(in) :: str

        character(len(str)) :: backstr, trimstr
        integer i, n

        trimstr = trim(str)
        n = len(trimstr)
        do i = 1, n
            backstr(i:i) = str(n - i + 1:n - i + 1)
        end do

    end function flip_text

!=============================================================================80
!
! Subroutine to write program options to a file
!
!=============================================================================80
    subroutine write_options(options)

        Use vardef, only: options_type

        type(options_type), intent(in) :: options

        character(300) :: suggname, filename
        character :: input
        logical woptdone, writefile, filecheck
        integer iunit

        woptdone = .false.
        writefile = .false.

        do while (.not. woptdone)

            suggname = trim(options%projectName)//'_settings.nml'
            write (*, *)
            write (*, *) 'Suggested file name: '//trim(suggname)
            write (*, *)
            write (*, *) 'Enter 1 to accept suggested name, 2 to enter new name, or'
            write (*, *) ' 3 to return to the main menu:'
            write (*, 1002)
            read (*, *) input

!   Get user input

            select case (input)

            case ('1')

                filename = suggname

            case ('2')

                write (*, *)
                write (*, *) 'Enter file name to write current program options:'
                write (*, 1002)
                read (*, *) filename

            case ('3')

                woptdone = .true.

            case default

                write (*, *)
                write (*, *) 'Error: input '//trim(input)//' not recognized.'
                write (*, *)
                woptdone = .true.

            end select

            if (.not. woptdone) then

!     Check for existence of file

                inquire (file=filename, exist=filecheck)
                if (filecheck) then
                    write (*, *)
                    write (*, *) 'Warning: '//trim(filename)//' already exists. Do you want'
                    write (*, *) ' to replace it (y/n)?'
                    write (*, 1002)
                    read (*, *) input
                    if (input=='y') then
                        woptdone = .true.
                        writefile = .true.
                    else
                        writefile = .false.
                    end if
                else
                    woptdone = .true.
                    writefile = .true.
                end if

            end if

!   Write out options

            if (writefile) then

                write (*, *)
                write (*, *) 'Writing current program options to '//trim(filename)//' ...'

                iunit = 12
                open (iunit, file=filename, status='replace')
                call write_options_file(iunit, options)
                close (iunit)
                woptdone = .true.

            end if

        end do

1002    format(/' Input > ', $)

    end subroutine write_options

!=============================================================================80
!
! Subroutine to write out file containing current program options
!
!=============================================================================80
    subroutine write_options_file(iunit, options)

        Use vardef, only: options_type

        type(options_type), intent(in) :: options
        integer, intent(in) :: iunit

! Surface options

        write (iunit, '(A)') '&SOPT'
        write (iunit, *) ' nsrf = ', options%nsrfdefault
        write (iunit, *) ' lesp = ', options%lesp
        write (iunit, *) ' tesp = ', options%tesp
        write (iunit, *) ' radi = ', options%radi
        write (iunit, *) ' nwke = ', options%nwake
        write (iunit, *) ' fdst = ', options%fdst
        write (iunit, *) ' fwkl = ', options%fwkl
        write (iunit, *) ' fwki = ', options%fwki
        write (iunit, '(A)') '/'

! Volume grid options

        write (iunit, '(A)') '&VOPT'
        write (iunit, *) " name = '"//trim(options%projectName)//"'"
        write (iunit, *) ' jmax = ', options%jmax
        write (iunit, *) " slvr = '"//trim(options%slvr)//"'"
        write (iunit, *) " topo = '"//options%topology//"'"
        write (iunit, *) ' ypls = ', options%yplus
        write (iunit, *) ' recd = ', options%Re
        write (iunit, *) ' cfrc = ', options%cfrac
        write (iunit, *) ' stp1 = ', options%maxsteps
        write (iunit, *) ' stp2 = ', options%fsteps
        write (iunit, *) ' nrmt = ', options%nrmt
        write (iunit, *) ' nrmb = ', options%nrmb
        write (iunit, *) ' alfa = ', options%alfa
        write (iunit, *) ' epsi = ', options%epsi
        write (iunit, *) ' epse = ', options%epse
        write (iunit, *) ' funi = ', options%funi
        write (iunit, *) ' asmt = ', options%asmt
        write (iunit, '(A)') '/'

! Grid output options

        write (iunit, '(A)') '&OOPT'
        write (iunit, *) ' gdim = ', options%griddim
        write (iunit, *) ' npln = ', options%nplanes
        write (iunit, *) ' dpln = ', options%spanwiseSpacing
        write (iunit, *) ' f3dm = ', options%fun3DCompatibility
        write (iunit, '(A)') '/'

    end subroutine write_options_file

end module util
