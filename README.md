# Aknowledgement

Parts of *structAirfoilMesher* are based on construct2d.

https://sourceforge.net/projects/construct2d

Copyright 2013 -- 2018 Daniel Prosser.

# License

*structAirfoilMesher* uses the GNU Public License Version 3, a copy of which can be
found in *LICENSE.md*.
# Introduction

*structAirfoilMesher* is a structured grid generator designed for 2D geometries.

The user can create and export meshes in various popular formats.

It can be used interactively, via its menu and command interface.

It can also be scripted/batched, by creating and utilizing simple input parameter files.

# Capabilities

## Original construct2d
+ elliptic and hyperbolic meshes
+ c-type and o-type topologies
+ 3D grids by simple extrusion on the third axis
+ plot3D output with additional nmf boundary condition file

## Primary modifications/additions of structAirfoilMesher
+ choice of various outputs that can be used directly or can be converted to popular formats
  + plot3D and corresponding nmf boundary condition file
  + structured VTK
  + structured/unstructured CGNS with boundary conditions (requires linking with CGNS library)
  + unstructured format for National Technical University of Athens in-house solver (MaPFlow)
+ option for multigrid output (multiple grid files are created)
+ option for swept 3D wings
+ improved Makefile that provides the choice for GNU, Nvidia, and Intel builds

# Building and usage instructions

Please refer to *INSTRUCTIONS.md*.
# Contributing

Please refer to *CONTRIBUTING.md*.

# Notes on output formats

## Plot3D

Plot3D was originally available in *Construct2d*.

It is a good option for structured meshes, though it lacks boundary conditions.
Therefore, in order to convert a Plot3D file to another format, extra work is needed.

Plot3D files also don't work particularly well with ParaView, which is my visualization software of choice.

## VTK

A simple format that can be viewed using literally any software 
(did Tecplot add vtk support? I don't know, it's been a while).

Very useful to just quickly load a created mesh and check if everything is ok. VTK files are handled very well by
ParaView and GMSH. 

However, like Plot3D, boundary conditions are kinda trivial and not well supported. Therefore, going from VTK to another format is not always easy.

Extra work is almost always required.

## MaPFlow ascii

This format is specific to solver *MaPFlow*, which is developed in the National University of Athens (and is the solver I worked on my PhD). 

It is a face-based ASCII format that is derived from the ANSYS Fluent format. 

If you don't work with that solver, you are not going to need this format. You can simply turn it off or leave the option out of your batch files (by default, the format is not written).

## CGNS

The CGNS format was chosen because it is supported by all CFD meshing software, at least to my knowledge.

I was looking for a widely used format that can also contain boundary conditions.

Unfortunately, I could not find any "simple" format that can contain boundary conditions:

+ Fluent format was possible, but I didn't want to follow that route since many open source meshers don't support it (i.e. SALOME). It is also hexadecimal, and I had trouble reading/writing hexadecimal from Fortran in the past.

+ Plot3d has the auxilliary .nmf file that defines boundaries. However, not all meshing software can utilize this file, which means that the users sometimes have to define boundaries on their own.

+ VTK cannot have boundary conditions. You can have boundary elements in different element sections, however most meshing programs that import VTK only utilize the volume element section.

+ Same for Tecplot files, only volume elements are imported. There is not a unique, non-trivial way to define boundary conditions.

I decided on CGNS because, in my mind, one could take the CGNS file, import it to the meshing software of his/her choice and (hopefully)
export it to any desired format.

The CGNS output file contains BCs defined on faces.
Thus it can be loaded and converted to different formats using any meshing software
(SALOME, ICEM CFD, ANSA), since all support CGNS.

# Sample mesh figures

![MESH](figs/naca0012OTypeAirfoil.png)
NACA0012 2D O-Type mesh

![MESH](figs/naca0012OTypeLE.png)
NACA0012 2D O-Type mesh, leading edge

![MESH](figs/naca0012OTypeTE.png)
NACA0012 2D O-Type mesh, trailing edge

![MESH](figs/naca0012CTypeAirfoil.png)
NACA0012 2D C-Type mesh

![MESH](figs/naca0012CTypeTE.png)
NACA0012 2D C-Type mesh, trailing edge

![MESH](figs/cylinderOType.png)
Cylinder 2D O-Type mesh

![MESH](figs/SD7003OType3D.png)
SD7003 3D O-Type mesh

![MESH](figs/SD7003OType3DSwept.png)
SD7003 3D O-Type mesh with a sweep angle of 45 degrees

![MESH](figs/SD7003OType3DSweptClustered.png)
SD7003 3D O-Type clustered mesh with a sweep angle of 45 degrees

![MESH](figs/naca235OTypeAirfoilLevel0.png)
NACA235 2D O-Type mesh created with 3 multigrid levels, level 0

![MESH](figs/naca235OTypeAirfoilLevel1.png)
NACA235 2D O-Type mesh created with 3 multigrid levels, level 1

![MESH](figs/naca235OTypeAirfoilLevel2.png)
NACA235 2D O-Type mesh created with 3 multigrid levels, level 2

![MESH](figs/naca235OTypeAirfoilLevel3.png)
NACA235 2D O-Type mesh created with 3 multigrid levels, level 3
