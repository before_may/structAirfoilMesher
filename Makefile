intel:
	make -f Makefile.structAirfoilMesher INTEL=1
intelDebug:
	make -f Makefile.structAirfoilMesher INTEL=1 DEBUG=1
intelCgns:
	make -f Makefile.structAirfoilMesher INTEL=1 CGNS=1
intelCgnsDebug:
	make -f Makefile.structAirfoilMesher INTEL=1 CGNS=1 DEBUG=1 

nvidia:
	make -f Makefile.structAirfoilMesher NVIDIA=1
nvidiaDebug:
	make -f Makefile.structAirfoilMesher NVIDIA=1 DEBUG=1
nvidiaCgns:
	make -f Makefile.structAirfoilMesher NVIDIA=1 CGNS=1
nvidiaCgnsDebug:
	make -f Makefile.structAirfoilMesher NVIDIA=1 CGNS=1 DEBUG=1 

gnu:
	make -f Makefile.structAirfoilMesher GNU=1
gnuDebug:
	make -f Makefile.structAirfoilMesher GNU=1 DEBUG=1
gnuCgns:
	make -f Makefile.structAirfoilMesher GNU=1 CGNS=1
gnuCgnsDebug:
	make -f Makefile.structAirfoilMesher GNU=1 CGNS=1 DEBUG=1 

cleanAll: clean cleanDebug

clean:
	make -f Makefile.structAirfoilMesher clean
cleanDebug:
	make -f Makefile.structAirfoilMesher cleanDebug
